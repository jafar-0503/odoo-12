{
    'name': 'Kendaraan Module',
    'version': '1.1.1',
    'summary': 'Model Kendaraan',
    'description': 'Model Kendaraan',
    'category': 'document',
    'author': 'equity',
    'website': 'www.equity.co.id',
    'license': 'AGPL-3',
    'depends': [
        'base'
    ],
    'data': [
        "view/kendaraan_view.xml",
        "security/ir.model.access.csv"
    ],
    'installable': True,
    'auto_install': False
}